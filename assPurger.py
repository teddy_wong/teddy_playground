#!/usr/bin/python

import datetime
import glob
import os
import re
import sys
import time

#from datadog import initialize, api


HOSTNAME="mon02.rodeofx.com"
DATADOG_API_KEY="452d25c2fd411b1bdb9ea12f05319d66"
DATADOG_APP_KEY="8d8545ab1bb29366490b9912097e52e81286e749"

NOW=time.time()
PID=os.getpid()
LOG_DIR = '/rodeo/setup/logs/assPurger/' + str(int(NOW)) + '_' + str(PID) + '/'
LOG_FILE=LOG_DIR + 'assPurger'
DRY_RUN=False

SHOWS_ROOT_DIR='/rdo/shows/'
SEVEN_DAYS_TIMESTAMP=604800

def sizeof_fmt(num, suffix='B'):
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)

def isValidShow(show):
	"""
	"""
	returnValue = True

	if 'tkflame' in show:
		returnValue = False
        elif 'bank' in show:
		returnValue = False
        elif 'rdo' in show:
		returnValue = False
        elif '-' in show:
		returnValue = False
        elif 'shows' in show:
		returnValue = False
        elif show.startswith('.'):
		returnValue = False

	return returnValue

def createLogDir():
	if not os.path.isdir(LOG_DIR):
		os.mkdir(LOG_DIR)

def main():
	
	showsList = os.listdir(SHOWS_ROOT_DIR)
	totalUsedSize = 0
	totalUsedSizeGT7Days = 0
	totalUsedShowSize = 0
	totalUsedShowSizeGT7days = 0

	for show in showsList:
                if isValidShow(show):
                        logFile = open(LOG_FILE + '.' + show, 'w')
                        totalUsedShowSize = 0
                        totalUsedShowSizeGT7days = 0
                        showOldestAssFile = NOW
                        showPath = SHOWS_ROOT_DIR + show

                        print "SHOW - " + show

                        for filename in glob.iglob(showPath +'/*/*/cg/maya/data/*/*.ass*'):

                                if '/_' not in filename:
                                        if re.search('_[0-9]{8}_[0-9]{6}/', filename) is not None:

                                                fileTimeStamp = os.path.getmtime(filename)
                                                fileSize = os.path.getsize(filename)
                                                totalUsedSize += fileSize
                                                totalUsedShowSize += fileSize
                                                dirPath = os.path.dirname(filename)

                                                if (NOW - fileTimeStamp) >= SEVEN_DAYS_TIMESTAMP:
                                                        fileDateString = '[' + datetime.datetime.fromtimestamp(fileTimeStamp).strftime('%Y-%m-%d %H:%M:%S') + '] '
                                                        totalUsedShowSizeGT7days += fileSize
                                                        totalUsedSizeGT7Days += fileSize

                                                        if DRY_RUN == False:
                                                                # Add delete command to remove file
                                                                logFile.write( fileDateString + filename + '\n' )
                                                                os.remove(filename)

                                                                # Remove top directory if empty
                                                                if os.listdir(dirPath) == []:
                                                                        logFile.write( fileDateString + dirPath + '\n' )
                                                                        os.rmdir(dirPath)
                                                        else:
                                                                logFile.write( 'DRY RUN ' + fileDateString + filename + '\n' )


                                                if fileTimeStamp < showOldestAssFile:
                                                        showOldestAssFile = fileTimeStamp

                        print "Show .ass* file usage: " + sizeof_fmt(totalUsedShowSize)
                        logFile.write(  "Show .ass* file usage: " + sizeof_fmt(totalUsedShowSize) + '\n')

                        print "Show .ass* file usage older then 7 Days: " + sizeof_fmt(totalUsedShowSizeGT7days)
                        logFile.write(  "Show .ass* file usage older then 7 Days: " + sizeof_fmt(totalUsedShowSizeGT7days) + '\n' )

                        print "Oldest ass file: " + datetime.datetime.fromtimestamp(showOldestAssFile).strftime('%Y-%m-%d %H:%M:%S')
                        logFile.write(  "Oldest ass file: " + datetime.datetime.fromtimestamp(showOldestAssFile).strftime('%Y-%m-%d %H:%M:%S') + '\n' )

                        print "--------\n"
                        logFile.close()

	print "Total usage of ass files: " + sizeof_fmt(totalUsedSize)
	print "Total usage of ass files greater then 7 days: " + sizeof_fmt(totalUsedSizeGT7Days)

#	api.Metric.send(metric='rodeo.internal.asspurger.storage.purged', points=totalUsedSizeGT7Days, host=HOSTNAME)
#	api.Metric.send(metric='rodeo.internal.asspurger.storage.used', points=(totalUsedSize - totalUsedSizeGT7Days), host=HOSTNAME)

if __name__ == "__main__":

	createLogDir()
	main()

	execTime = time.time() - NOW
#	api.Metric.send(metric='rodeo.internal.asspurger.executiontime', points=exectime, host=HOSTNAME)

