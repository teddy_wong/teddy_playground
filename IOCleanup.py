#!/use/bin/python
import datetime
import glob
import os
import smtplib
import time

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

DRY_RUN=True
SHOWS_ROOT_DIR='/rdo/shows/'
#SHOWS_ROOT_DIR='/mnt/nexfs/shows/shows/'
IN_MAX_TIMESTAMP=2592000
DELIVERY_MAX_TIMESTAMP=2592000
TEMP_MAX_TIMESTAMP=1209600
NOW = time.time()


# EMAIL
ME='servicedesk@rodeofx.com'


def sizeof_fmt(num, suffix='B'):
	"""
	"""	
	for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
		if abs(num) < 1024.0:
	    		return "%3.1f%s%s" % (num, unit, suffix)
		num /= 1024.0

	return "%.1f%s%s" % (num, 'Yi', suffix)

def get_size(start_path = '.'):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
	    if not os.path.islink(fp):
                total_size += os.path.getsize(fp)
    return total_size

def getShowsOnNetwork():
	"""
	"""
	showsList = os.listdir(SHOWS_ROOT_DIR)
	returnValue = []

	for show in showsList:
		if isValidShow(show):
			returnValue.append( show )

	return returnValue

def isValidShow(show):
        """
        """
        returnValue = True

        if 'tkflame' in show:
                returnValue = False
        elif 'bank' in show:
                returnValue = False
        elif 'rdo' in show:
                returnValue = False
        elif '-' in show:
                returnValue = False
        elif 'shows' in show:
                returnValue = False
        elif show.startswith('.'):
                returnValue = False

        return returnValue

def tree(dir, padding, printFiles = False, logFile = None):
    if logFile is not None:
	log = open(logFile, 'w')
	
    if logFile is not None: 
	log.write( padding[:-1] + '+-' + os.path.basename(os.path.abspath(dir)) + '/\n' )
    else:
	print padding[:-1] + '+-' + os.path.basename(os.path.abspath(dir)) + '/'

    padding = padding + ' '
    files = []
    if printFiles:
        files = os.listdir(dir)
    else:
        files = [x for x in os.listdir(dir) if os.path.isdir(dir + os.sep + x)]
    count = 0
    for file in files:
        count += 1
	if logFile is not None: 
	    log.write( padding + '|\n' )
	else:
       	    print padding + '|'

        path = dir + os.sep + file
        if os.path.isdir(path):
            if count == len(files):
                tree(path, padding + ' ', printFiles, logFile)
            else:
                tree(path, padding + '|', printFiles, logFile)
        else:
	    if logFile is not None: 
		log.write( padding + '+-' + file + '\n')
            else:
		print padding + '+-' + file


def takeAction(path):
	logDir = os.path.dirname(path) + '/_logs/'
	logDir = '/var/tmp/'
	logFile = logDir + os.path.basename(path) + '.log'
	

	if '/_io/_out/delivery/' in path:
		tree(path, ' ', True, logFile) 
	elif '/_io/_out/_temp/' in path:
		print '_temp'
	elif '/_io/_in/' in path:
		print '_in'

	return True	
	#if not DRY_RUN:
		#if '_out' in path:
			#os.removedirs(path)
		#if '_in' in path:
		#move to punch01

def scanIoInOut(show, subPath, maxTimeStamp):
	totalShowDeliverySize = 0
	
	for path in glob.iglob(SHOWS_ROOT_DIR + show + subPath + '*'):
		pathTimeStamp = os.path.getmtime(path)

		if NOW - pathTimeStamp >= maxTimeStamp:
			if os.path.isdir(path) and not os.path.basename(path).startswith('_logs'):
				pathSize = get_size(path) 
				totalShowDeliverySize += pathSize
				takeAction(path)


	return totalShowDeliverySize

def main():
	totalDeliverySize = 0 
	shows = getShowsOnNetwork()

	shows = [ 'shk' ]
	for show in shows: 
		showDeliverySize = scanIoInOut( show, '/_io/_out/delivery/', DELIVERY_MAX_TIMESTAMP )
		totalDeliverySize += showDeliverySize
		print "Show " + show + "_out/delivery size: " + sizeof_fmt(showDeliverySize)

		showDeliverySize = scanIoInOut( show, '/_io/_out/_temp/', TEMP_MAX_TIMESTAMP )
		totalDeliverySize += showDeliverySize

		print "Show " + show + "_out/_temp size: " + sizeof_fmt(showDeliverySize)

		showDeliverySize = scanIoInOut( show, '/_io/_in/', IN_MAX_TIMESTAMP )
		totalDeliverySize += showDeliverySize

		print "Show " + show + " _io/_in size: " + sizeof_fmt(showDeliverySize)
	
	
	print "Total delivery size: " + sizeof_fmt(totalDeliverySize)	

if __name__ == "__main__":
	main()
	
